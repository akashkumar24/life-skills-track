# Object-Oriented Programming (OOP)
Object-Oriented Programming is a programming paradigm based on the concept of "objects". It is a way of organizing and designing code that emphasizes the use of objects and their interactions to solve problems. OOP is used in many programming languages, including JavaScript, which is the language we will focus on in this report.

## Concepts of OOP

There are several key concepts that are important to understand when working with OOP in JavaScript:


### 1) Classes

A class is a blueprint or template for creating objects. It defines the properties and methods that all objects created from that class will have. In JavaScript, classes are defined using the class keyword.

* example

`class Animal {
  constructor(name) {
    this.name = name;
  }
  
  speak() {
    console.log(this.name + ' makes a noise.');
  }
}`

let myAnimal = new Animal('Cat');
myAnimal.speak(); // output: "Cat makes a noise."

### 2) Objects

An object is an instance of a class. It has its own set of properties and methods that are defined by the class. In JavaScript, objects are created using the new keyword.

* example

`let myAnimal = new Animal('Cat');`

### 3) Encapsulation

Encapsulation is the practice of keeping data and methods that operate on that data within the same object. This is done to prevent external code from accessing or modifying the data directly. In JavaScript, this can be achieved using private and public properties and methods.

* example

`class Person {
  #age = 0; // private property
  
  get age() { // public method
    return this.#age;
  }
  
  set age(value) { // public method
    if (value < 0 || value > 120) {
      throw new Error('Invalid age.');
    }
    this.#age = value;
  }
}`

let person = new Person();
person.age = 30; // set age using public method
console.log(person.age); // get age using public method

In this example, we define a Person class using the class syntax introduced in ECMAScript 2015. This class has a private property #age which can only be accessed from within the Person class itself. We also define a public method age using the get and set keywords, which can be used to access and modify the value of the private #age property from outside the class.

When we create an instance of the Person class using let person = new Person(), the #age property is initialized to zero by default. We can then use the public age method to set and get the value of the #age property.

For example, person.age = 30 sets the value of the #age property to 30 using the set method defined in the Person class. This method checks whether the value is within a valid range (between 0 and 120 in this case) and throws an error if it is not.

Similarly, console.log(person.age) gets the value of the #age property using the get method defined in the Person class. This method simply returns the value of the #age property.

By using private properties and public methods, we can encapsulate the implementation details of the Person class and provide a clear and well-defined interface to the user, which is a key aspect of abstraction in object-oriented programming.

### 4) Inheritance

Inheritance is the practice of creating a new class based on an existing class. The new class inherits all the properties and methods of the existing class and can add its own properties and methods as well. In JavaScript, inheritance is achieved using the extends keyword.

* example

`class Cat extends Animal {
  constructor(name, breed) {
    super(name);
    this.breed = breed;
  }
  
  speak() {
    console.log(this.name + ' meows.');
  }
  
  purr() {
    console.log(this.name + ' purrs.');
  }
}`

let myCat = new Cat('Fluffy', 'Persian');
myCat.speak(); // output: "Fluffy meows."
myCat.purr(); // output: "Fluffy purrs."


In this example, we define a Cat class that extends the Animal class using the extends keyword. This means that the Cat class inherits all the properties and methods of the Animal class, and can also define its own additional properties and methods.

The Cat class has two properties, name and breed. The name property is inherited from the Animal class through the super(name) call in the constructor, which passes the name parameter to the Animal constructor. The breed property is specific to the Cat class and is initialized using the breed parameter passed to the Cat constructor.

The Cat class also has two methods, speak and purr. The speak method outputs a message indicating that the cat is meowing, and the purr method outputs a message indicating that the cat is purring. Both methods use the name property to refer to the cat's name.

When we create an instance of the Cat class using let myCat = new Cat('Fluffy', 'Persian'), we pass two parameters to the Cat constructor to set the name and breed properties of the cat. We can then use the speak and purr methods to make the cat meow and purr, respectively.

For example, myCat.speak() outputs the message "Fluffy meows.", because this.name refers to the value of the name property of the myCat instance, which is set to "Fluffy". Similarly, myCat.purr() outputs the message "Fluffy purrs."

### 5) Abstraction

In JavaScript, abstraction is typically achieved using classes and objects.We can define a class that contains properties and methods, which are used to abstract away the implementation details and provide a clear and well-defined interface to the user.

* example

`class BankAccount {
  constructor(accountNumber, balance) {
    this.accountNumber = accountNumber;
    this.balance = balance;
  }

  deposit(amount) {
    this.balance += amount;
  }

  withdraw(amount) {
    if (this.balance >= amount) {
      this.balance -= amount;
    } else {
      console.log('Insufficient funds');
    }
  }`

  getBalance() {
    return this.balance;
  }
}

In this example, BankAccount is a class that provides an abstraction for a bank account. It has three methods: deposit(), withdraw(), and getBalance(). These methods provide a clear and well-defined interface to the user, while hiding the implementation details of how the bank account works.

For example, the deposit() method allows the user to deposit a certain amount of money into the bank account, while the withdraw() method allows the user to withdraw a certain amount of money from the bank account, as long as there are sufficient funds available. If the user tries to withdraw more money than is available, the method will return an error message. Finally, the getBalance() method allows the user to check the current balance of the bank account.

By providing this clear and well-defined interface, the BankAccount class allows the user to interact with the bank account in a simple and intuitive way, without having to worry about the implementation details of how the bank account works. This is the essence of abstraction in JavaScript and OOP in general.


### 6) Polymorphism

Polymorphism is the practice of using a single interface to represent multiple types of objects. In JavaScript, polymorphism can be achieved through inheritance and method overriding.

* code sample

`class Shape {
  area() {
    console.log('I don\'t know how to calculate my area.');
  }
}

class Rectangle extends Shape {
  constructor(width, height) {
    super();
    this.width = width;
    this.height = height;
  }
  
  area() {
    console.log('Rectangle area:', this.width * this.height);
  }
}

class Circle extends Shape {
  constructor(radius) {
    super();
    this.radius = radius;
  }
  
  area() {
    console.log('Circle area:', Math.PI * this.radius * this.radius);
  }
}

let shapes = [new Rectangle(10, 5), new Circle(3)];
shapes`

 code sample demonstrates polymorphism in JavaScript. The Shape class serves as the base class for the Rectangle and Circle classes. The Shape class has a method called area() which is implemented in the derived classes. Each derived class provides its own implementation of the area() method that calculates the area of the shape it represents. This allows the area() method to be called on an object of the Shape class, which can then execute the appropriate implementation based on the type of the object.

In the code sample, an array of Shape objects is created, which includes a Rectangle and a Circle object. When the area() method is called on each object in the array, the appropriate implementation is executed. The Rectangle object's area() method calculates the area of the rectangle using the width and height properties, while the Circle object's area() method calculates the area of the circle using the radius property.

This is an example of how polymorphism allows for code to be more flexible and adaptable, as the area() method can be called on any Shape object and the appropriate implementation will be executed based on the object's type. This makes it easier to write code that can handle multiple types of objects without having to check the type of each object explicitly.

### References Section


- https://www.javatpoint.com/java-oops-concepts
- https://www.w3schools.com/java/java_oop.asp

-  https://www.geeksforgeeks.org/object-oriented-programming-oops-concept-in-java/






